회원 모델 사용 시나리오

1. 아이디 로그인

settings.py

```
AUTH_USER_MODEL = 'account.User'
AUTH_USERNAME_FIELD = 'username'
AUTH_REQUIRED_FIELDS = ['last_name', 'first_name', ]
```

models.py

```
email = models.EmailField(max_length=255, unique=False, )
username = models.CharField(max_length=150, unique=True, )
```

2. 이메일 주소 로그인

settings.py

```
AUTH_USER_MODEL = 'account.User'
AUTH_USERNAME_FIELD = 'email'
AUTH_REQUIRED_FIELDS = ['username', 'last_name', 'first_name', ]
```

models.py

```
email = models.EmailField(max_length=255, unique=True, )
username = models.CharField(max_length=150, unique=False, )
```

3. 이메일 인증 2단계 가입 처리

settings.py

```
ACCOUNT_ACTIVATION_DAYS = 1  # Enables 2-phase registration
```

4. 소셜 로그인 처리

settings.py

```
# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'social_django', # <- 끝에 추가
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware', # <- 끝에 추가
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends', # <- 끝에 추가
                'social_django.context_processors.login_redirect', # <- 끝에 추가
            ],
            'debug': DEBUG,
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.twitter.TwitterOAuth',
    'social_core.backends.line.LineOAuth2',
    'social_core.backends.kakao.KakaoOAuth2',
    'django.contrib.auth.backends.ModelBackend', # <- 맨 끝에 Django 디폴트 백엔드가 위치
)

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    # 'social_core.pipeline.mail.mail_validation',
    # 'social_core.pipeline.social_auth.associate_by_email',
    # 'social_core.pipeline.user.create_user', # <- 파이프라인 생성 함수 무시
    'account.social.create_user', # <- 사용자 생성 함수 호출
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

# 페이스북 예시
SOCIAL_AUTH_FACEBOOK_KEY = Secret.SOCIAL_AUTH_FACEBOOK_KEY
SOCIAL_AUTH_FACEBOOK_SECRET = Secret.SOCIAL_AUTH_FACEBOOK_SECRET
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']  # Fetch email address
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id, name, email, last_name, first_name',
}
```

urls.py

```
urlpatterns = [
    url(r'^oauth/',
        include('social_django.urls', namespace='social')),

    url(r'^accounts/register/$',
        RegistrationView.as_view(form_class=WebUserCreationForm), name='registration_register'),
    url(r'^accounts/',
        include('registration.backends.hmac.urls')),
]
```

social.py

```
USER_FIELDS = ['email', 'username', 'last_name', 'first_name', ]


def create_user(strategy, details, backend, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name, details.get(name)))
                  for name in backend.setting('USER_FIELDS', USER_FIELDS))
    if not fields:
        return

    return {
        'is_new': True,
        'user': strategy.create_user(**fields)
    }
```

login.html

```
<a href="{% url 'social:begin' 'facebook' %}">Login with Facebook</a>
<a href="{% url 'social:begin' 'google-oauth2' %}">Login with Google</a>
<a href="{% url 'social:begin' 'twitter' %}">Login with Twitter</a>
<a href="{% url 'social:begin' 'line' %}">Login with Line</a>
<a href="{% url 'social:begin' 'kakao' %}">Login with Kakao</a>
```

주의사항

* 이메일 주소를 반환하지 않는 경우가 있기 때문에 이메일 로그인 시스템에서는 일부 회원의 로그인 처리가 안 된다.